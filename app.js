//[SECTION] Dependencies and Modules

const express = require("express");
const mongoose = require("mongoose");

//[SECTION] Server Setup

const app = express(); //establish connection
const port = 4000; //define address where app is served

//[SECTION] DB Connection

mongoose.connect('mongodb+srv://natcorpuz:admin123@cluster0.yso0q.mongodb.net/toDo176?retryWrites=true&w=majority', {
    //avoids deprecation warnings because of connection issues
    useNewUrlParser: true,
    useUnifiedTopology: true
}); //connect to mongoDB Atlas

//get creds from atlas user

//create notif if db connection is success/fail

let db = mongoose.connection;

//add on() method from mongoose connection to show if connection succeeded/failed in terminal and browser(client)

db.on('error', console.error.bind(console, "Connection Error"));

//once connection is successful, display msg in terminal

db.once('open', () => console.log("connected to MongoDB"));

//middleware - methods in express context, a function that acts and adds features to app

app.use(express.json());

//Schema
/*  
    - before creating docs from api to store in database, we must determine structure of documents to be written in our database, to ensure consistency of our documents and avoid future errors
    - schema acts as a blueprint for data/doc
    - schema() constructor from mongoose to create new schema object
*/

const taskSchema = new mongoose.Schema({
    /* 
        - define fields for docs
        - determine appropriate data type
    */
    
    name: String,
    status: String

});

//Mongoose Model
/* 

    - models are for connecting the api to corresponding collection in database
    - representation of collection
    - models uses schemas to create object that correspond to schema
    - by default, when creating the collection from model, collection name is pluralized

    - direct connection to your collection

    mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)
*/

const Task = mongoose.model("task", taskSchema);

//POST route to create new task
app.post('/tasks', (req, res) => {
	/* 
    when creating a new post/put or any route that requires data from the client, first console log your req.body or any part of the request that contains the data */

	/*
        - create new task doc
        - constructor should follow schema of model 
    */
    
	let newTask = new Task({
		name: req.body.name,
		status: req.body.status,
	});

	/* save()
        - save() method from an object created by a model.
        - save() method will allow us to save doc by connecting to collection via model 
        - save() has 2 approaches: 
            1. add anonymous func to handle created doc or error
            2. add .then() chain to handle errors and created docs in seperate functions 

        SYNTAX: 
            newTask.save((err, savedTask) => {
                if (err) {
                    res.send(err)
                } else {
                    res.send(savedTask)
                }
            })
    */

	/* .then(), .catch()
        .then() and .catch() chain

        .then() === handles proper result/returned value of function
            - if function returns a proper value, we can use a separate function to handle it
        
        .catch() === handles errors from use of function
            - if error occurs, we can handle error separately

        SYNTAX:
            newTask.save()
                .then(result => res.send(result))
                .catch(error => res.send(error));
    */

	newTask
		.save()
		.then((result) => res.send({ message: "document creation success" }))
		.catch((error) => res.send({ message: "error in document creation" }));
});

/* sample schema */

const sampleSchema = new mongoose.Schema({
    name: String,
    isActive: Boolean
});

const Sample = mongoose.model("samples", sampleSchema);

app.post('/samples', (req, res ) => {
    let newSample = new Sample({
        name: req.body.name,
        isActive: req.body.isActive
    });

    newSample.save((savedSample, error) => {
        if (error) {
            res.send(error);
        } else {
            res.send(savedSample);
        }
    });
}); 

/* sample schema 2 (tagalog) */
/* 
const manggagamitSchema = new mongoose.Schema({
    username: String,
    isAdmin: Boolean
});

const Manggagamit = mongoose.model("manggagamit", manggagamitSchema);

app.post('/mgamanggagamit', (req, res) => {
    let newManggagamit = new Manggagamit({
        username: req.body.username,
        isAdmin: req.body.isAdmin
    });

    newManggagamit.save((error, savedManggagamit) => {
        if (error) {
            res.send(error);
        } else {
            res.send(savedManggagamit);
        }
    });
});
 */

/* GET method */

app.get('/tasks', (req, res) => {
    //to query using mongoose, first access the model of the collection you want to manipulate
    //Model.find() in mongoose is similar in function to mongoDB's db.collection.find()
    //mongodb - db.tasks
    Task.find({})
        .then(result => res.send(result))
        .catch(error => res.send(error));
});

/* mini activity 1
    create new GET request method route to get all sample documents
    send the array of sample documents in our postman client
    else, catch error and send error to client
*/

app.get('/samples', (req, res) => {
    Sample.find({})
        .then(result => res.send(result))
        .catch(error => res.send(error));
});

//[SECTION] Entry Point Response

app.listen(port, () => {
    console.log(`Server active on port ${port}`)
}); //bind connection to port

/* ACTIVITY */

const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

const User = mongoose.model("users", userSchema);

app.post('/users', (req, res) => {
    let newUser = new User({
        username: req.body.username,
        password: req.body.password
    });

    newUser.save()
        .then(result => res.send(result))
        .catch(err => res.send(err));
});

app.get('/users', (req, res) => {
    User.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err));
});