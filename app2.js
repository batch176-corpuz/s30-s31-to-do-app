//COMMENT-LESS VIEW

const express = require("express");
const mongoose = require("mongoose");

const app = express(); //establish connection
const port = 4000; //define address where app is served

mongoose.connect('mongodb+srv://natcorpuz:admin123@cluster0.yso0q.mongodb.net/toDo176?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log("connected to MongoDB"));

app.use(express.json());


////
const taskSchema = new mongoose.Schema({
    
    name: String,
    status: String

});
const Task = mongoose.model("task", taskSchema);
app.post('/tasks', (req, res) => {
	
	let newTask = new Task({
		name: req.body.name,
		status: req.body.status,
	});

	newTask
		.save()
		.then((result) => res.send({ message: "document creation success" }))
		.catch((error) => res.send({ message: "error in document creation" }));
});
app.get("/tasks", (req, res) => {
	Task.find({})
		.then((result) => res.send(result))
		.catch((error) => res.send(error));
});


////
const sampleSchema = new mongoose.Schema({
    name: String,
    isActive: Boolean
});
const Sample = mongoose.model("samples", sampleSchema);
app.post('/samples', (req, res ) => {
    let newSample = new Sample({
        name: req.body.name,
        isActive: req.body.isActive
    });

    newSample.save((savedSample, error) => {
        if (error) {
            res.send(error);
        } else {
            res.send(savedSample);
        }
    });
}); 
app.get('/samples', (req, res) => {
    Sample.find({})
        .then(result => res.send(result))
        .catch(error => res.send(error));
});


////
const userSchema = new mongoose.Schema({
    username: String,
    password: String
});
const User = mongoose.model("users", userSchema);
app.post('/users', (req, res) => {
    let newUser = new User({
        username: req.body.username,
        password: req.body.password
    });

    newUser.save()
        .then(result => res.send(result))
        .catch(err => res.send(err));
});
app.get('/users', (req, res) => {
    User.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err));
});


/////////
app.listen(port, () => {
	console.log(`Server active on port ${port}`);
}); 