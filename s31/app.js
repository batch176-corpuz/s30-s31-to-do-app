/* mini activity
    - setup basic express JS server
*/

//dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

//server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect(
	"mongodb+srv://natcorpuz:admin123@cluster0.yso0q.mongodb.net/toDo176?retryWrites=true&w=majority",
	{
		//avoids deprecation warnings because of connection issues
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB"));

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Server running on port ${port}`));