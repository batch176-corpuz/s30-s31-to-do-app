/* Controllers ReadMe
    - contains functions and business logic of Express app
*/

const Task = require("../models/task");

/* [CONTROLLERS] */


/* CREATE 
*/
module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        name: requestBody.name,
    });
    return newTask.save().then((task, error) => {
        if (error) {
            return false;
        } else {
            return task;
        }
    })
};

/* READ
*/
module.exports.getAllTasks = () => {
    return Task.find({})
        .then(result => result)
};

/* UPDATE 
    - change status of task from pending > completed
    - reference a doc with ObjectID
*/
module.exports.taskCompleted = (taskID) => {
    /*my method 
    */
    // return Task.findByIdAndUpdate(
    //      taskID,
    //     { status: "completed" })
    //     .then((found, err) => {
    //         if (found) {
    //             return 'Task updated';
    //         } else {
    //             return 'Task failed to update';
    //         }
    //     });

    /* my method v2
    */
    // return Task.findByIdAndUpdate(taskID,
    //     { status: "completed" })
    //     .then(result => 'update success')
    //     .catch(err => 'update failed');

    /* sir marty method
     */
    return Task.findById(taskID)
        .then((found, err) => {
            if (found) {
                found.status = 'completed';
                return found.save()
                    .then((updated, saveErr) => {
                        if (updated) {
                            return 'Task update success';
                        } else {
                            return 'Task update fail';
                        }
                    });
            } else {
                return `no match`;
            }
        });
    };
    
    /* UPDATE
    */
module.exports.taskPending = (taskID) => {
    return Task.findById(taskID)
        .then((found, err) => {
            if (found) {
                found.status = 'pending';
                return found.save()
                    .then((updated, saveErr) => {
                        if (updated) {
                            return 'Task update success';
                        } else {
                            return 'Task update fail';
                        }
                    });
            } else {
                return `no match`;
            }
        });
}

/* DELETE 
*/
module.exports.deleteTask = (taskID) => {
    //built-in from mongoose
    //long version compared to READ above
    return Task.findByIdAndRemove(taskID)
        .then((fulfilled, rejected) => {
            if (fulfilled) {
                return 'Task removed';
            } else {
                return 'Task not removed';
            }
        });
};


/* PROMISES
    3 states
        - pending: waiting to be executed
        - fulfilled: successfully executed
        - rejected: unfulfilled promise
    
    a 'thenable' expression
        - returns value to append .then()
        - code inside runs if fulfilled/rejected
        - syntax:
            - thenableExpression()
                .then(fulfilled, rejected) => {})
*/