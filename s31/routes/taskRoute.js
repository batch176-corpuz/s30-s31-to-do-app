/* [README]
    - contains all endpoints for app
    - separate routes such that "app.js" only contains info on the server
*/

const express = require("express");

const taskController = require("../controllers/taskControllers");


/* [ROUTES]
    - create a Router instance that functions as a middleware and routing system
    - allows access to HTTP method middlewares that makes it easier to create routes for our application
*/
const router = express.Router();


/* [CREATE] a task
 */
router.post("/", (req, res) => {
    taskController.createTask(req.body)
        .then(resultFromController => res.send(resultFromController));
});


/* [READ] all tasks
 */
router.get("/", (req, res) => {
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

/* [UPDATE] a task to complete 
    - contains PATH VARIABLE ":" --> dynamic route
    - accessed through "req.params.<urlname>"
*/
router.put("/:task", (req, res) => {
    let taskID = req.params.task;

    taskController.taskCompleted(taskID)
        .then(outcome => res.send(outcome));

    //my method
    // taskController.taskCompleted(taskID)
    //     .then(result => res.send(result));
})

/* [UPDATE] a task to pending 
*/
router.put("/:task/pending", (req, res) => {
    let taskID = req.params.task;

    taskController.taskPending(taskID)
        .then(result => res.send(result));

    // res.send(taskID);
});



/* [DELETE] a task 
*/
router.delete("/:task", (req, res) => {
    let taskID = req.params.task;

    /* returns promise v1
    */
    // taskController.deleteTask(taskID)
    //      .then((result => res.send(result))); 
    
    /* returns promise v2 
    */
    taskController.deleteTask(taskID)
        .then((fulfill, reject) => {
        if (fulfill) {
            res.send(fulfill);
        } else {
            res.send(reject);
        }
    })
})

/* Use "module.exports" to export the router object to use in "app.js" 
*/

module.exports = router;


/* PATH VARIABLE
    - inserts data within URL
    - useful when inserting single info
    - useful when passing info to REST API that does NOT include a BODY section like 'GET'
    - when integrating a path variable, you are also changing the behavior of the path from STATIC to DYNAMIC

    2 TYPES OF ENDPOINTS
        - Static route
            - unchanging endpoint
        - Dynamic route
            - changing endpoint
*/

/* __v --> VERSION CONTROL KEY 
    - describes how many times a resource is updated
*/